all: poetry_collection

poetry_collection: cover-a.pdf cover-b.pdf poetry_collection.tex
	pdflatex poetry_collection.tex
	pdflatex poetry_collection.tex

cover-a.pdf: cover/cover-a.svg
	rsvg-convert -f pdf cover/cover-a.svg -o cover/cover-a.pdf

cover-b.pdf: cover/cover-b.svg
	rsvg-convert -f pdf cover/cover-b.svg -o cover/cover-b.pdf

clean:
	find -name *.pdf -exec rm -f {} \+
	find -name *.log -exec rm -f {} \+
	find -name *.aux -exec rm -f {} \+
	find -name *.out -exec rm -f {} \+
	find -name *.toc -exec rm -f {} \+
