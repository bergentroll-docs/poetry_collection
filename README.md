# Стихи и песни

Небольшой сборник моего творчества за разные годы.

Подготовлен при помощи дистрибутива TeX Live. Доступен под лицензией
[CC0-1.0 ](https://creativecommons.org/publicdomain/zero/1.0/).

[Ссылка на последнюю версию](
https://bergentroll-docs.gitlab.io/poetry_collection/poetry_collection.pdf).
